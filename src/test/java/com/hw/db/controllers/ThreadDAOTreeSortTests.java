package com.hw.db.controllers;

import com.hw.db.DAO.PostDAO;
import com.hw.db.DAO.ThreadDAO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.Optional;
import java.util.stream.Stream;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class ThreadDAOTreeSortTests {

    private static JdbcTemplate mockJdbc;
    @BeforeEach
    void init() {
        mockJdbc = mock(JdbcTemplate.class);
        new ThreadDAO(mockJdbc);
    }

    private static Stream<Arguments> testCases() {
        return Stream.of(
                Arguments.of(1, 1, 1, true, "SELECT * FROM \"posts\" WHERE thread = ?  AND branch < (SELECT branch  FROM posts WHERE id = ?)  ORDER BY branch DESC  LIMIT ? ;"),
                Arguments.of(1, 1, 1, null, "SELECT * FROM \"posts\" WHERE thread = ?  AND branch > (SELECT branch  FROM posts WHERE id = ?)  ORDER BY branch LIMIT ? ;")
        );
    }

    @ParameterizedTest
    @MethodSource("testCases")
    void testTreeSort(Integer id, Integer limit, Integer since, Boolean desc, String expected) {
        ThreadDAO.treeSort(id, limit, since, desc);
        verify(mockJdbc)
                .query(Mockito.eq(expected),
                    Mockito.any(PostDAO.PostMapper.class),
                    Optional.ofNullable(Mockito.any())
                );
    }


}