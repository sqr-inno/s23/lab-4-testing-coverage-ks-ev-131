package com.hw.db.controllers;

import static org.mockito.Mockito.verify;
import java.util.stream.Stream;

import com.hw.db.DAO.ForumDAO;
import com.hw.db.DAO.UserDAO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

class ForumDAOUserListTests {
  JdbcTemplate mockJdbc;
  ForumDAO forumDAO;

  @BeforeEach
  void init() {
    mockJdbc = Mockito.mock(JdbcTemplate.class);
    forumDAO = new ForumDAO(mockJdbc);
  }

  private static Stream<Arguments> testCases() {
    return Stream.of(
      Arguments.of(
        "slug1",
        null,
        null,
        null,
        "SELECT username,fullname,email,about FROM forum_users WHERE forum = (?)::citext ORDER BY username;"
      ),
      Arguments.of(
        "slug2",
        null,
        "since",
        null,
        "SELECT username,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  username > (?)::citext ORDER BY username;"
      ),
      Arguments.of(
        "slug3",
        null,
        "since",
        true,
        "SELECT username,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  username < (?)::citext ORDER BY username desc;"
      ),
      Arguments.of(
        "slug4",
        null,
        null,
        true,
        "SELECT username,fullname,email,about FROM forum_users WHERE forum = (?)::citext ORDER BY username desc;"
      ),
      Arguments.of(
        "slug5",
        1,
        null,
        true,
        "SELECT username,fullname,email,about FROM forum_users WHERE forum = (?)::citext ORDER BY username desc LIMIT ?;"
      ),
      Arguments.of(
        "slug6",
        1,
        "since",
        true,
        "SELECT username,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  username < (?)::citext ORDER BY username desc LIMIT ?;"
      ),
      Arguments.of(
        "slug7",
        1,
        "since",
        null,
        "SELECT username,fullname,email,about FROM forum_users WHERE forum = (?)::citext AND  username > (?)::citext ORDER BY username LIMIT ?;"
      ),
      Arguments.of(
        "slug8",
        1,
        null,
        null,
        "SELECT username,fullname,email,about FROM forum_users WHERE forum = (?)::citext ORDER BY username LIMIT ?;"
      )
    );
  }

  @ParameterizedTest
  @MethodSource("testCases")
  void testUserListAllBranches(
    String slug,
    Number limit,
    String since,
    Boolean desc,
    String expected
  ) {
    ForumDAO.UserList(slug, limit, since, desc);
    verify(mockJdbc)
      .query(
        Mockito.eq(expected),
        Mockito.any(Object[].class),
        Mockito.any(UserDAO.UserMapper.class)
      );
  }
}
