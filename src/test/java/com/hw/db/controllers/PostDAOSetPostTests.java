package com.hw.db.controllers;

import com.hw.db.DAO.PostDAO;
import com.hw.db.models.Post;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import java.sql.Timestamp;
import java.util.Optional;
import java.util.stream.Stream;

import static org.mockito.Mockito.*;

public class PostDAOSetPostTests {

    private static JdbcTemplate mockJdbc;
    private static final Integer postId = 0;

    @BeforeEach
    void init() {
        mockJdbc = mock(JdbcTemplate.class);
        new PostDAO(mockJdbc);
    }

    private static Stream<Arguments> testCases() {
        return Stream.of(
                Arguments.of(postId, "newAuthor", "oldPostMessage", new Timestamp(0), "UPDATE \"posts\" SET  author=?  , isEdited=true WHERE id=?;"),
                Arguments.of(postId, "oldAuthor", "oldPostMessage", new Timestamp(1), "UPDATE \"posts\" SET  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"),
                Arguments.of(postId, "oldAuthor", "newPostMessage", new Timestamp(0), "UPDATE \"posts\" SET  message=?  , isEdited=true WHERE id=?;"),
                Arguments.of(postId, "newAuthor", "newPostMessage", new Timestamp(0), "UPDATE \"posts\" SET  author=?  ,  message=?  , isEdited=true WHERE id=?;"),
                Arguments.of(postId, "oldAuthor", "newPostMessage", new Timestamp(1), "UPDATE \"posts\" SET  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"),
                Arguments.of(postId, "newAuthor", "oldPostMessage", new Timestamp(1), "UPDATE \"posts\" SET  author=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"),
                Arguments.of(postId, "newAuthor", "newPostMessage", new Timestamp(1), "UPDATE \"posts\" SET  author=?  ,  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;")
        );
    }

    @ParameterizedTest
    @MethodSource("testCases")
    void testSetPostUpdate(Integer id, String author, String message, Timestamp created, String expected) {
        Post post = new Post();
        post.setId(postId);
        post.setAuthor("oldAuthor");
        post.setMessage("oldPostMessage");
        post.setCreated(new Timestamp(0));
        Mockito.when(mockJdbc.queryForObject(
                Mockito.eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"),
                Mockito.any(PostDAO.PostMapper.class),
                Mockito.eq(postId))
        ).thenReturn(post);

        Post newPost = new Post();
        newPost.setId(id);
        newPost.setAuthor(author);
        newPost.setMessage(message);
        newPost.setCreated(created);

        PostDAO.setPost(postId, newPost);

        verify(mockJdbc).update(Mockito.eq(expected), Optional.ofNullable(Mockito.any()));
    }

    @Test
    void testSetPostTestId() {
        Post newPost = new Post();
        PostDAO.setPost(postId, newPost);
        verify(mockJdbc, never()).update(Mockito.any(), Optional.ofNullable(Mockito.any()));
    }

    @Test
    void testSetPostUpdateNotCalled2() {
        Post post = new Post();
        post.setId(postId);
        post.setAuthor("oldAuthor");
        post.setMessage("oldPostMessage");
        post.setCreated(new Timestamp(0));

        Mockito.when(mockJdbc.queryForObject(
                Mockito.eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"),
                Mockito.any(PostDAO.PostMapper.class),
                Mockito.eq(postId))
        ).thenReturn(post);

        PostDAO.setPost(postId, post);
        verify(mockJdbc, times(0)).update(Mockito.any(), Optional.ofNullable(Mockito.any()));
    }

}