package com.hw.db.controllers;

import com.hw.db.DAO.UserDAO;
import com.hw.db.models.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.Optional;
import java.util.stream.Stream;

import static org.mockito.Mockito.*;

public class UserDAOChangeTests {
    private static JdbcTemplate jdbcMock;

    @BeforeEach
    void init() {
        jdbcMock = mock(JdbcTemplate.class);
        new UserDAO(jdbcMock);
    }

    private static Stream<Arguments> testCases() {
        return Stream.of(
                Arguments.of("username1", "email", null, null, "UPDATE \"users\" SET  email=?  WHERE username=?::CITEXT;"),
                Arguments.of("username2", "email", "fullName", null, "UPDATE \"users\" SET  email=? , fullname=?  WHERE username=?::CITEXT;"),
                Arguments.of("username3", "email", null, "about", "UPDATE \"users\" SET  email=? , about=?  WHERE username=?::CITEXT;"),
                Arguments.of("username4", null, "fullName", null, "UPDATE \"users\" SET  fullname=?  WHERE username=?::CITEXT;"),
                Arguments.of("username5", null, "fullName", "about", "UPDATE \"users\" SET  fullname=? , about=?  WHERE username=?::CITEXT;"),
                Arguments.of("username6", null, null, "about", "UPDATE \"users\" SET  about=?  WHERE username=?::CITEXT;"),
                Arguments.of("username7", "email", "fullName", "about", "UPDATE \"users\" SET  email=? , fullname=? , about=?  WHERE username=?::CITEXT;")
        );
    }

    @Test
    void UserDAOChangeTestName() {
        User user = new User("username8", null, null, null);
        UserDAO.Change(user);
        verify(jdbcMock, never()).update(Mockito.any(), Optional.ofNullable(Mockito.any()));
    }

    @ParameterizedTest
    @MethodSource("testCases")
    void UserDAOChange(String username, String email, String fullName, String about, String expected) {
        User user = new User(username, email, fullName, about);
        UserDAO.Change(user);
        verify(jdbcMock).update(Mockito.eq(expected), Optional.ofNullable(Mockito.any()));
    }

}